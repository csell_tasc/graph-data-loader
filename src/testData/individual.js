import * as moment from 'moment';
import createRandom from '../utils/random';
import mockPhone from './helpers/createPhoneNumber';

export default clientId => ({
  newIndividual: {
    clientId,
    firstName: 'Jimmy',
    middleName: 'sandwich',
    lastName: 'John',
    employmentInfo: {
      hireDate: '12/1/2012', // `${moment().toISOString()}`,
      employmentStatus: 'Employed',
      payrollScheduleName: 'Weekly',
    },
    addresses: [
      {
        addressType: 'PrimaryResidence',
        addressLine1: `1234 Main Street`,
        city: 'city town',
        state: 'WI',
        zip: createRandom(11111, 99999),
      },
    ],
    contactInfo: {
      contactType: 'Primary',
      name: 'Jimmy John',
      emails: [
        {
          address: 'me@me.net',
          isPrimary: true,
        },
        {
          address: 'me@YOLO.net',
          isPrimary: false,
        },
      ],
      phones: [
        {
          number: mockPhone(),
          isPrimary: true,
        },
        {
          number: mockPhone(),
          isPrimary: false,
        },
      ],
    },
  },
});
