import createRandom from '../utils/random';

export default individualId => ({
  newBankAccount: {
    individualId,
    name: 'Main Checking Account',
    accountType: 'Checking',
    accountNumber: createRandom(111111111, 999999999),
    routingNumber: createRandom(111111111, 999999999),
    bankName: 'JP Morgan Chase',
  },
});
