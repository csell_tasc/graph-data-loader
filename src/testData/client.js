import createRandom from '../utils/random';
import mockPhone from './helpers/createPhoneNumber';

export default {
  newClient: {
    name: 'Big Ole Company',
    ein: '12-1231234',
    filingStatus: 'LLC',
    allowCard: true,
    allowDisbursement: true,
    allowEligibilityClasses: true,
    offerAdvanceAccount: false,
    addresses: [
      {
        addressType: 'HeadQuarters',
        addressLine1: '1234 Main Street',
        city: 'city town',
        state: 'WI',
        zip: `${createRandom(11111, 99999)}`,
      },
      {
        addressType: 'CorporateAddress',
        addressLine1: '1234 Other Main Street',
        city: 'Village Place',
        state: 'WI',
        zip: `${createRandom(11111, 99999)}`,
      },
    ],
    contactInfo: {
      contactType: 'Primary',
      name: 'Jimmy John',
      emails: [
        {
          address: 'me@me.net',
          isPrimary: true,
        },
        {
          address: 'me@mememe.net',
          isPrimary: false,
        },
      ],
      phones: [
        {
          number: mockPhone(),
          isPrimary: true,
        },
        {
          number: mockPhone(),
          isPrimary: false,
        },
      ],
    },
  },
};
