import createRandom from '../../utils/random';

export default () => `${createRandom(111, 999)}-${createRandom(111, 999)}-${createRandom(1111, 9999)}`;
