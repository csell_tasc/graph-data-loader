import logger from './utils/logger';
import * as bankAccount from './mutators/bankAccount';
import * as individual from './mutators/individual';
import * as client from './mutators/client';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'; // ignore our self signed cert.

const arg = process.argv.slice(2);
switch (arg[0]) {
  case '--createStack':
    client.create(2);
    break;

  case '--delAll':
    Promise.all([
      client.deleteAllClients(),
      bankAccount.deleteAllAccounts(),
      individual.deleteAllAccounts(),
    ]).then(result => logger.data(result));
    break;

  case '--delClients':
    client.deleteAllClients();
    break;

  case '--delBankAccounts':
    bankAccount.deleteAllAccounts();
    break;

  case '--delIndividual':
    individual.deleteAllAccounts();
    break;

  case '--createIndividual':
    individual.create(1);
    break;

  default:
    logger.error(`please pass in an argument. Was passed: ${arg}`);
    break;
}
