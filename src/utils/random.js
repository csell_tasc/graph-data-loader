export default (min, max) => {
  const result = Math.random() * (max - min) + min;
  return Math.round(result);
};
