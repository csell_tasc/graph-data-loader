import * as moment from 'moment';
import { createApolloFetch } from 'apollo-fetch';
import logger from '../utils/logger';
import individualTestData from '../testData/individual';
import * as bankAccount from './bankAccount';

import * as options from '../options';

const { uri } = options;
const apolloFetch = createApolloFetch({ uri });

const queries = {
  createNew: `
    mutation($newIndividual: IndividialInputType!){
      createIndividual(individual: $newIndividual)
    }
  `,
  getAll: `query{individuals{id}}`,
  deleteById: `
    mutation($userId: String!) {
      deleteIndividualById(id: $userId)
    }
  `,
};

export const deleteAllAccounts = () => {
  const promises = [];

  return new Promise((resolve, reject) => {
    apolloFetch({ query: queries.getAll }).then(result => {
      result.data.individuals.forEach(r => {
        const variables = { userId: r.id };
        promises.push(
          apolloFetch({ query: queries.deleteById, variables }).then(dr => {
            logger.info(dr);
          })
        );
      });
    });

    Promise.all(promises)
      .then(r => resolve(r))
      .catch(err => reject(err));
  });
};

const postData = (query, variables) =>
  new Promise((resolve, reject) => {
    apolloFetch({ query, variables }) // all apolloFetch arguments are optional
      .then(result => {
        logger.data(result);
        return resolve(result);
      })
      .catch(error => {
        logger.error(error);
        return reject(error);
      });
  });

export const create = (numToCreate, clientId = `unassigned`) => {
  const promises = [];
  const counter = 15;
  return new Promise((resolve, reject) => {
    for (let i = 0; i < counter; i += 1) {
      const vars = individualTestData(clientId);
      promises.push(
        postData(queries.createNew, vars).then(r => {
          bankAccount.createAssociatedAccount(r.data.createIndividual, 2);
        })
      );
    }

    Promise.all(promises)
      .then(values => resolve(values))
      .catch(err => reject(err));
  });
};
