import { createApolloFetch } from 'apollo-fetch';
import logger from '../utils/logger';

import * as options from '../options';

import clientTestData from '../testData/client';
import * as individual from './individual';

const { uri } = options;
const apolloFetch = createApolloFetch({ uri });

const queries = {
  createNew: `
    mutation($newClient: ClientInputType!){
        createClient(client: $newClient)
    }
  `,
  getAll: `query{clients{id, name}}`,
  deleteById: `
    mutation($id: String!) {
      deleteClientById(id: $id)
    }
  `,
};

export const deleteAllClients = () => {
  const promises = [];
  return new Promise((resolve, reject) => {
    apolloFetch({ query: queries.getAll }).then(result => {
      result.data.clients.forEach(r => {
        const variables = { id: r.id };
        promises.push(
          apolloFetch({ query: queries.deleteById, variables }).then(dr => {
            logger.info(dr);
          })
        );
      });
    });

    Promise.all(promises)
      .then(r => resolve(r))
      .catch(err => reject(err));
  });
};

const postData = (query, variables) =>
  new Promise((resolve, reject) => {
    apolloFetch({ query, variables }) // all apolloFetch arguments are optional
      .then(result => {
        logger.data(result);
        return resolve(result);
      })
      .catch(error => {
        logger.error(error);
        return reject(error);
      });
  });

export const create = (numToCreate = 1) => {
  const promises = [];
  return new Promise((resolve, reject) => {
    for (let i = 0; i < numToCreate; i += 1) {
      promises.push(
        postData(queries.createNew, clientTestData).then(r => {
          individual.create(numToCreate, r.data.createClient);
        })
      );
    }

    Promise.all(promises)
      .then(values => resolve(values))
      .catch(err => reject(err));
  });
};
