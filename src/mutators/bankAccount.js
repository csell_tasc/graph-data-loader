import { createApolloFetch } from 'apollo-fetch';
import logger from '../utils/logger';
import bankAccountTestData from '../testData/bankAccounts';

import * as options from '../options';

const { uri } = options;
const apolloFetch = createApolloFetch({ uri });

const queries = {
  createNew: `
    mutation($newBankAccount: BankAccountInputType!){
        createNewBankAccount(bankAccount: $newBankAccount)
    }`,
  getAll: `query{bankAccounts{id, individualId}}`,
  deleteById: `
      mutation($accountId: String!, $individualId: String!) {
      deleteBankAccountById(id: $accountId, individualId: $individualId)
    }`,
};
// Mutation CHANGED
export const deleteAllAccounts = () => {
  const promises = [];

  return new Promise((resolve, reject) => {
    apolloFetch({ query: queries.getAll }).then(result => {
      result.data.bankAccounts.forEach(account => {
        const variables = {
          accountId: account.id,
          individualId: account.individualId,
        };
        promises.push(
          apolloFetch({ query: queries.deleteById, variables }).then(r => {
            logger.info(r);
          })
        );
      });
    });

    Promise.all(promises)
      .then(r => resolve(r))
      .catch(err => reject(err));
  });
};

const postData = (query, variables) =>
  new Promise((resolve, reject) => {
    apolloFetch({ query, variables }) // all apolloFetch arguments are optional
      .then(result => {
        logger.data(result);
        return resolve(result);
      })
      .catch(error => {
        logger.error(error);
        return reject(error);
      });
  });

export const createAssociatedAccount = (individualId, numToCreate = 1) =>
  new Promise((resolve, reject) => {
    for (let index = 0; index < numToCreate; index += 1) {
      const vars = bankAccountTestData(individualId);
      logger.data(vars);
      postData(queries.createNew, vars)
        .then(r => resolve(r))
        .catch(err => reject(err));
    }
  });
