import { createApolloFetch } from 'apollo-fetch';

import logger from '../utils/logger';
import * as options from '../options';

const { uri } = options;
const apolloFetch = createApolloFetch({ uri });

const fetch = (query, variables) =>
  new Promise((resolve, reject) => {
    apolloFetch({ query, variables }) // all apolloFetch arguments are optional
      .then(result => {
        logger.data(result);
        return resolve(result);
      })
      .catch(error => {
        logger.error(error);
        return reject(error);
      });
  });

const create = (numToCreate, query, variables) => {
  const promises = [];
  return new Promise((resolve, reject) => {
    for (let i = 0; i < numToCreate; i += 1) {
      promises.push(fetch(query, variables));
    }

    Promise.all(promises)
      .then(values => resolve(values))
      .catch(err => reject(err));
  });
};

// export const delete = (query) => {
// const promises = [];

// return new Promise((resolve, reject) => {
//     apolloFetch({ query: query }).then(result => {
//     result.data.bankAccounts.forEach(account => {
//         const variables = { accountId: account.id };
//         promises.push(
//         apolloFetch({ query: queries.deleteById, variables }).then(r => {
//             logger.info(r);
//         })
//         );
//     });
//     });

//     Promise.all(promises)
//     .then(r => resolve(r))
//     .catch(err => reject(err));
// });
// };
